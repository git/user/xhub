# Copyright 1999-2008 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header$

DESCRIPTION="Tools for IPv6 networking"
HOMEPAGE="http://www.remlab.net/ndisc6/"
SRC_URI="http://www.remlab.net/files/ndisc6/${P}.tar.bz2"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"

DEPEND="dev-lang/perl sys-devel/gettext"

src_compile() {
	econf --localstatedir=/var || die "econf failed"
	emake || die "emake failed"
}

src_install() {
	make DESTDIR="${D}" install || die "make install failed"
	dodoc README ChangeLog NEWS AUTHORS INSTALL
#	newinitd "${FILESDIR}"/rdnssd.rc rdnssd
#	newconfd "${FILESDIR}"/rdnssd.conf rdnssd
#	exeinto /etc/rdnssd
#	doexe "${FILESDIR}"/resolvconf
}
