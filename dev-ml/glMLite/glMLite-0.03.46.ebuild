# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

DESCRIPTION="OpenGL bindings for OCaml"
HOMEPAGE="http://www.linux-nantes.org/~fmonnier/OCaml/GL/"
SRC_URI="http://www.linux-nantes.org/~fmonnier/OCaml/GL/download/${P}.tgz"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="media-libs/libpng
media-libs/jpeg
media-libs/freeglut
virtual/opengl
gnome-base/librsvg
media-libs/ftgl
media-libs/gle" # check this
RDEPEND="${DEPEND}"

src_compile() {
	emake -j1
}

src_install() {
	emake -j1 DESTDIR="${D}" PREFIX="${D}"/`ocamlc -where`/${PN} install
}
