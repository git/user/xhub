# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=3

inherit findlib

DESCRIPTION="Xmlm is an OCaml module providing streaming XML input/output"
HOMEPAGE="http://erratique.ch/software/xmlm"
SRC_URI="http://erratique.ch/software/xmlm/releases/${P}.tbz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc ocamlopt"

DEPEND="${DEPEND}
>=dev-lang/ocaml-3.10[ocamlopt?]"
RDEPEND="${DEPEND}"

src_compile() {
	./build module-byte

	if use ocamlopt; then
		./build module-native
	fi

	if use doc; then
		./build doc
	fi
}

src_install() {
	export INSTALLDIR=${D}/`ocamlc -where`/xmlm

	./build install-byte
	if use ocamlopt; then
		./build install
	else
		./build install-byte
	fi
}
