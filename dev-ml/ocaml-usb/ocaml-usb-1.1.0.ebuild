# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=2

inherit findlib oasis eutils

DESCRIPTION=""
HOMEPAGE="https://forge.ocamlcore.org/projects/ocaml-usb/"
SRC_URI="https://forge.ocamlcore.org/frs/download.php/540/${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE="doc examples"

DEPEND="${DEPEND}
>=dev-lang/ocaml-3.11
>=dev-libs/libusb-1.0
>=dev-ml/lwt-2.0.0"
RDEPEND="${DEPEND}"

src_configure() {
	chmod +x configure
	oasis_src_configure
}

src_compile() {
	emake || die "make failed"

	if use doc; then
		emake doc || die "failed to build the documentation"
	fi
}

src_install() {
	findlib_src_install
	if use doc; then
	    dohtml _build/usb.docdir/*.html
	fi
	if use examples; then
		insinto /usr/share/doc/${PF}/examples
		doins examples/*
	fi
}
